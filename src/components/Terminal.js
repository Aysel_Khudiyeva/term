import React from 'react';
import Iframe from './Iframe';
import Table from './Table';


const Terminal = () => {
    return (

       <div className="terminal-body">
           <Iframe />
           <Table />
       </div>
        
    )

}

export default Terminal;